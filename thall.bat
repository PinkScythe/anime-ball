for /f "tokens=*" %%a in ('dir /b /s *.html') do tidy --doctype html5 --output-html yes --char-encoding utf8 --newline lf --merge-divs no --clean yes --indent auto --vertical-space yes --tidy-mark no --wrap 0 --write-back yes "%%a" | echo file: %%a
@echo .
@echo .
@dir *.html /o:d
